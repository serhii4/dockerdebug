FROM gradle:5.4-jdk11 as builder
USER root
WORKDIR /builder
ADD . /builder
CMD ["./gradlew build", "-x", "test", "--stacktrace"]
#RUN gradle build -x test --stacktrace

FROM openjdk:11-jre
WORKDIR /app
EXPOSE 8080
EXPOSE 5005
COPY --from=builder /builder/build/libs/*.jar /app/app.jar
#moved to docker-compose.yml
#CMD ["java", "-Djava.security.egd=file:/dev/./urandom", "-Dblabla", "-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5005", "-jar", "/app/app.jar"]