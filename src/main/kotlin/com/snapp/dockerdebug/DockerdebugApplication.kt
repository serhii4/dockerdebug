package com.snapp.dockerdebug

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class DockerdebugApplication

fun main(args: Array<String>) {
	runApplication<DockerdebugApplication>(*args)
}
