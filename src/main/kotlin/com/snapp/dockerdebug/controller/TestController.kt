package com.snapp.dockerdebug.controller

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class TestController {

    @GetMapping(value = "/a")
    fun a(): String {
        val a = "test"
        println("hello, ${a}")
        return "a"
    }

    @GetMapping(value = "/b")
    fun b(): String = "b"
}